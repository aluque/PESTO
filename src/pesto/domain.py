import numpy as np
import scipy.constants as co

class Domain():
    SAVED_VARS = ['n', 'm', 'l', 'Lr', 'Lz']
    REDUCED_VARS = {'n', 'm', 'l'}

    # Our physical domain is n x m but we add l cells to all 4 sides to act as
    # ghost cells / CPML layer.  This is slightly wasteful because we need only
    # 2 ghost cells but it greatly simplifies the code to pass only 3 integers
    # to all functions and having these numbers be always the same.
    #
    # To improve alignment and simplify the code all arrays will have the same
    # size even if some of the data is unused.
    # The largest size of any array is (n + 2l + 1) x (m + 2l + 1).
    # We use this size to initialize all arrays and make sure that
    # They are multiples of 32.
    def __init__(self, n, m, l, Lr, Lz):
        self.n = n
        self.m = m
        self.l = l
        self.Lr = Lr
        self.Lz = Lz

        self.dr = self.Lr / n
        self.dz = self.Lz / m
        self.dt = min(self.dr, self.dz) / (np.sqrt(3) * co.c)
        
        self.nx, self.mx = n + 2 * l + 1, m + 2 * l + 1 

        self.rf = np.linspace(0, Lr, n + 1)
        self.rc = 0.5 * (self.rf[1:] + self.rf[:-1])
        
        self.zf = np.linspace(0, Lz, m + 1)
        self.zc = 0.5 * (self.zf[1:] + self.zf[:-1])

        self.rfx = np.linspace(-self.l * self.dr, Lr + self.l * self.dr,
                               n + 2 * l + 1)
        self.rcx = 0.5 * (self.rfx[1:] + self.rfx[:-1])

        self.zfx = np.linspace(-self.l * self.dz, Lz + self.l * self.dz,
                               m + 2 * l + 1)
        self.zcx = 0.5 * (self.zfx[1:] + self.zfx[:-1])

        self.data_reduction = 1
        
        
    def init_cpml(self, dtype=np.float64):
        from . import cuda

        l = self.l
        dr = self.dr
        dt = self.dt
        
        # Initialize data for the CPML.
        Z = np.sqrt(co.mu_0 / co.epsilon_0)
        smax = -(l + 1) * np.log(1e-6) / (2 * Z * l * dr)

        drcl   = (np.arange(l) + 0.5) * dr
        drfl   = (np.arange(l) +   1) * dr

        sigmarc = smax * (drcl / (l * dr))**4.0
        sigmarf = smax * (drfl / (l * dr))**4.0

        bc_host = np.exp(-sigmarc * dt / co.epsilon_0)
        ac_host = bc_host - 1;

        bf_host = np.exp(-sigmarf * dt / co.epsilon_0)
        af_host = bf_host - 1;

        self.ac = cuda.gpu.to_gpu(ac_host.astype(dtype))
        self.bc = cuda.gpu.to_gpu(bc_host.astype(dtype))
        self.af = cuda.gpu.to_gpu(af_host.astype(dtype))
        self.bf = cuda.gpu.to_gpu(bf_host.astype(dtype))

        
    def zeros_host(self, dtype=np.float64):
        return np.zeros((self.nx, self.mx), dtype)

    
    def zeros_dev(self, dtype=np.float64):
        from . import cuda

        return cuda.gpu.zeros((self.nx, self.mx), dtype)


    def zeros_dev_l(self, dtype=np.float64):
        from . import cuda

        return cuda.gpu.zeros((self.l, self.mx), dtype)


    def zeros_host_l(self, dtype=np.float64):
        return np.zeros((self.l, self.mx), dtype)
    
     
    def save(self, group):
        for var in self.SAVED_VARS:
            if self.data_reduction > 1 and var in self.REDUCED_VARS:
                group.attrs[var] = int(getattr(self, var) / self.data_reduction)
            else:
                group.attrs[var] = getattr(self, var)
        
        group.attrs['data_reduction'] = self.data_reduction

    
    @staticmethod
    def load(group):
        kwargs = {}
        for var in Domain.SAVED_VARS:
            kwargs[var] = group.attrs[var]

        return Domain(**kwargs)


    def pretty_print(self):
        print("Domain :")
        print("         n     = %-4d" % self.n)
        print("         m     = %-4d" % self.m)
        print("         l     = %-4d" % self.l)
        print("         Lr    = %-12g m" % self.Lr)
        print("         Lz    = %-12g m" % self.Lz)
        print("         dr    = %-12g m" % self.dr)
        print("         dz    = %-12g m" % self.dz)
        print("         dt    = %-12g s" % self.dt)

        
