""" Code for the Near-to-far-field transform. """

import numpy as np
import scipy.constants as co
from .contexttimer import ContextTimer

ETA_0 = np.sqrt(co.mu_0 / co.epsilon_0)

class NTFF(object):
    def __init__(self, dom, robs, zobs, tend, ntheta=None, dtype=np.float64):
        self.dom = dom
        self.robs = robs
        self.zobs = zobs
        self.tend = tend
        self.ntheta = ntheta
        self.dtype = dtype
        self.blk = (256, 1, 1)
        
        
    def init(self):
        from . import cuda
        import skcuda.linalg as linalg
        
        mod = cuda.modules[self.dtype]
        
        Lz = self.dom.Lz
        Lr = self.dom.Lr
        n, m, l = self.dom.n, self.dom.m, self.dom.l
        dr, dz, dt = self.dom.dr, self.dom.dz, self.dom.dt
        robs, zobs = self.robs, self.zobs
        ntheta = self.ntheta
        dtype = self.dtype
        blk = self.blk
        
        if ntheta is None:
            ntheta = Lr * 2 * np.pi / dz

        # These are the shortest and longest delays from any source point
        # in the domain boundary to the observation point.
        min_delay = np.sqrt(((Lz / 2) - zobs)**2 + (robs - Lr)**2) / co.c
        max_delay = np.sqrt(zobs**2 + (robs + Lr)**2) / co.c

        # This is the number of observations time points affected by a given
        # state of the source fields.
        tlen = int(np.ceil(max_delay / dt) - np.floor(min_delay / dt))

        # Timestep shifts for the n-valued and n+1/2-valued fields.
        k = np.floor(min_delay / dt)
        k1 = np.floor(min_delay / dt + 0.5) - 0.5

        self.k = k
        self.k1 = k1
        self.tlen = tlen
        
        self.ezobs = cuda.gpu.zeros(int(np.ceil((self.tend + max_delay) / dt)),
                               dtype)
        self.tobs = np.arange(self.ezobs.shape[0]) * dt
        
        self.au = cuda.gpu.zeros((tlen, m + 1), self.dtype)
        self.aw = cuda.gpu.zeros((tlen, m + 1), self.dtype)

        self.Up = cuda.gpu.zeros((tlen, 1), self.dtype)
        self.Wp = cuda.gpu.zeros((tlen, 1), self.dtype)

        with ContextTimer("Calculating NTFF matrices (%d x %d)" % (tlen, m + 1)):
            mod.calc_ntff(np.int32(n), np.int32(m), np.int32(l),
                          np.int32(tlen), dtype(dt * k), np.int32(ntheta),
                          np.int32(0),
                          self.aw,
                          dtype(robs), dtype(zobs - Lz / 2),
                          dtype(dt), dtype(dr), dtype(dz),
                          block=blk,
                          grid=(int(np.ceil((m + 1) / blk[0])), 1, 1))
            
            mod.calc_ntff(np.int32(n), np.int32(m), np.int32(l),
                          np.int32(tlen), dtype(dt * k1), np.int32(ntheta),
                          np.int32(1),              
                          self.au,
                          dtype(robs), dtype(zobs - Lz / 2),
                          dtype(dt), dtype(dr), dtype(dz),
                          block=blk,
                          grid=(int(np.ceil((m + 1) / blk[0])), 1, 1))
        
            cuda.context.synchronize()

        linalg.init()

        
    def apply(self, t_h, t_e, ez, hphi):    
        import skcuda.linalg as linalg

        n, m, l = self.dom.n, self.dom.m, self.dom.l
        tlen = self.tlen
        dtype = self.dtype
        
        ez1 = 0.5 * (ez[l + n - 1, l: l + m + 1] + ez[l + n, l: l + m + 1])
        hphi1 = hphi[l + n, l: l + m + 1]
        
        linalg.dot(self.au, ez1.reshape((m + 1, 1)).astype(dtype),
                   out=self.Up)
        linalg.dot(self.aw, hphi1.reshape((m + 1, 1)).astype(dtype),
                   out=self.Wp)

        dt = self.dom.dt
        k = self.k
        k1 = self.k1
        
        Wi = int(round((t_h - dt / 2 + dt * k) / dt))
        Ui = int(round((t_e - dt / 2 + k1 * dt) / dt))

        self.ezobs[Wi: Wi + tlen] += -ETA_0 * self.Wp.reshape((tlen,))
        self.ezobs[Ui: Ui + tlen] += self.Up.reshape((tlen,))
        

    def dump(self, group):
        group.create_dataset('aw', data=self.aw.get(),
                             compression='gzip')
        group.create_dataset('au', data=self.aw.get(),
                             compression='gzip')

