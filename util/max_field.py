import sys
import os
import glob

from numpy import *
import h5py

from pesto import Domain, Solution, Variable

def main():
    path = sys.argv[1]
    for fname in glob.glob(os.path.join(path, "[0-9]*.h5")):
            fp = h5py.File(fname)

            dom = Domain.load(fp['domain'])
            sol = Solution(dom)
            sol.load(fp)
            time = fp.attrs['t']
            fp.close()

            _, _, eabs = Variable.INDEX['eabs'](sol)

            print("%g %g # %s" % (time, amax(eabs), fname))


if __name__ == '__main__':
    main()
