PESTO
=====

PESTO is the Parallel Electromagnetic and Self-consistent Transport
Open-source code.  It solves electron transport and electromagnetic wave
propagation self-consistently.  The code is implemented using CUDA to achieve
large-scale parallelism.


Requirements
------------

The following software is required to use PESTO:

* CUDA version 8.0 or higher.
* NumPy, SciPy.
* PyCUDA
* scikit CUDA (only for the NTFF tansform to compute far-fields).
* h5py.
* PyYAML.


Installation
------------

Install PESTO by cloning this repository:
```
  git clone git@gitlab.com:aluque/PESTO.git
```

You must compile the CUDA binary to run a simulation.  To do so move to
the `src/pesto` folder and type
```
  make
```
for this compilation you need the CUDA compiler.

To run PESTO you must set up your `PATH` and `PYTHONPATH` environment
variables:
```
  PATH="$PATH:your/path/to/pesto/bin"
  PYTHONPATH="$PYTHONPATH:your/path/to/pesto/src"
```

Running
-------

Run a simulation using e.g.

```
  pesto-run streamer_collision input.yaml
```

This will start the application `streamer_collision` reading the input data
from `input.yaml`.  For a description of all input parameters, look at
the file `src/pesto/apps/streamer_collision.py`.  The folder `src/pesto/apps/`
contains some other *apps* that can be invoked by `pesto-run`.








