#! env python3

import sys
import os
from shutil import copy2

import numpy as np
import scipy.constants as co

from pesto import Domain, Solution, Controller, Photoionization
from pesto.param import (param, ParameterContainer, ParameterNotAllowed,
                         contained_in)

def main():
    ###
    ### ARRANGE INPUT / OUTPUT
    ###
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("input",
                        help="The input file (.yaml/.json/.h5)")

    args = parser.parse_args()

    params = Parameters()

    params.file_load(args.input)
    params.report()

    rid = os.path.splitext(args.input)[0]

    out_dir = os.path.join(os.path.split(os.path.abspath(args.input))[0], rid)
    try:
        os.mkdir(out_dir)
    except OSError:
        pass

    copy2(args.input, out_dir)
    params.json_dumpf(os.path.join(out_dir, 'parameters.json'))

    ###
    ### SET SIMULATION PARAMETERS
    ###
    
    n, m, l = params.n, params.m, params.l
    Lz = params.Lz
    Lr = params.Lr if params.Lr > 0 else Lz * n / m
    
    if (n + 2 * l + 1) % 32 != 0:
        raise ValueError("(n + 2 * l + 1) must be multiple of 32 ")

    if abs(m * Lr / (n * Lz) - 1) > 1e-5:
        print("(m * Lr / (n * Lz)) = %g" % abs(m * Lr / (n * Lz)))
        raise ValueError("At the moment, only allowing dr = dz")

    
    dom = Domain(n, m, l, Lr, Lz)
    dom.pretty_print()
    
    # Initialize electron density
    ne = dom.zeros_host()
    l = params.l
    ne[l:-l - 1, l:-l - 1] += (params.max_init_dens_upper *
                               gaussian(dom.rc, dom.zc,
                                        0,
                                        params.seed_upper,
                                        params.width_upper,
                                        +1))

    ne[l:-l - 1, l:-l - 1] += (params.max_init_dens_lower
                               * gaussian(dom.rc, dom.zc,
                                          0,
                                          params.seed_lower,
                                          params.width_lower,
                                          -1))

    ne[l:-l - 1, l:-l - 1] += params.background_ionization
    
    t0, t1, dt = params.time_range
    st0, st1, sdt = params.time_slow_range

    full_time = np.r_[t0:t1:dt]
    flt = np.logical_or(full_time < st0, full_time >= st1)
    full_time = full_time[flt]
    slow_time = np.r_[st0:st1:sdt]
    
    out_times = np.sort(np.r_[full_time, slow_time])

    ###
    ### RUN THE SIMULATION
    ###
    dtype = np.dtype(params.dtype).type
    sol = Solution(dom, dtype=dtype)
    sol.setall(ne=ne, ez=params.background_electric_field)
    sol.to_gpu()

    photo = None
    if params.photoionization:
        photo = Photoionization(dom, pO2=params.oxygen_partial_p,
                                dtype=dtype)
        photo.to_gpu()
    
    cont = Controller(dom, sol, photo=photo)
    
    cont.init_output(out_dir)
    
    dom.data_reduction = params.data_reduction
    sol.data_reduction = params.data_reduction
    
    cont.run(out_times)

    
def gaussian(rc, zc, r0, z0, width, direction):
    r1 = (rc - r0)[:, None]
    z1 = (zc - z0)[None, :]

    return np.exp(-r1**2 / width**2 - z1**2 / width**2)


class Parameters(ParameterContainer):
    @param(default="")
    def family(s):
        """ A descriptor for a family of runs. """
        return str(s)
    
    @param()
    def n(s):
        """ Number of grids cells in the r dimension. """
        return int(s)

    @param()
    def m(s):
        """ Number of grids cells in the r dimension. """
        return int(s)

    @param()
    def l(s):
        """ Number of CPML cells. """
        return int(s)
        
    @param()
    def Lz(s):
        """ Domain extension in the z dimension. """
        return float(s)

    @param()
    def Lr(s):
        """ Domain extension in the r dimension. """
        return float(s)
    
    @param(default=1e21)
    def max_init_dens_upper(s):
        """ Peak electron density in the initial upper seed. """
        return float(s)

    @param(default=1e21)
    def max_init_dens_lower(s):
        """ Peak electron density in the initial lower seed. """
        return float(s)
    
    @param(default=.2 * co.milli)
    def width_upper(s):
        """ Width of the upper seed. """
        return float(s)

    @param(default=.2 * co.milli)
    def width_lower(s):
        """ Width of the lower seed. """
        return float(s)
    
    @param()
    def seed_upper(s):
        """ Seed location (z) of the upper channel. """
        return float(s)

    @param()
    def seed_lower(s):
        """ Seed location (z) of the upper channel. """
        return float(s)
    
    @param()
    def background_ionization(s):
        """ Background electron density. """
        return float(s)

    @param()
    def background_electric_field(s):
        """ External electric field. """
        return float(s)

    @param(default=False)
    def photoionization(s):
        """ Is photo-ionization included? """
        return s

    @param(default=150 * co.torr)
    def oxygen_partial_p(s):
        """ Oxygen partial pressure """
        return s

    @param(contained_in({'float64', 'float32'}), default='float64')
    def dtype(s):
        """ The precision type (float32/float64). """
        return s

    @param(default=1)
    def data_reduction(s):
        """ Reduce data output this factor in each dimension. """
        v = int(s)
        if 32 % v != 0:
            raise ParameterNotAllowed("data_reduction must divide 32.")

        return v
    
    @param(default=[0, 0, 1])
    def time_slow_range(s):
        """ Interval of slow-motion saving in the form start:end:step. """
        v = [float(x) for x in s.split(':')]

        if len(v) != 3:
            raise ParameterNotAllowed("Use start:end:step for time ranges")

        return v
    
    @param()
    def time_range(s):
        """ Time range of the full simulation. """
        v = [float(x) for x in s.split(':')]

        if len(v) != 3:
            raise ParameterNotAllowed("Use start:end:step for time ranges")

        return v


if __name__ == '__main__':
    main()


