#! /bin/bash
# Script to start a simulation in the qsub queue
# Alejandro Luque - 2017

PYTHON_EXEC=python3
UNAME_FULL=`uname -a`
DATE=`date`

echo "# [${DATE}]"
echo "# ${UNAME_FULL}"

echo "> nvidia-smi"
nvidia-smi

export PYTHONPATH="$PYTHONPATH:$HOME/projects/pesto/src"
export PATH="$PATH:/usr/local/cuda-8.0/bin/:$HOME/projects/pesto/bin"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/cuda-8.0/lib:$HOME/anaconda3/lib"

~/cuda/NVIDIA_CUDA-8.0_Samples/1_Utilities/deviceQuery/deviceQuery
dmesg |grep NVRM


export TEMP=/tmp/
export TMPDIR=/tmp/
export TEMP=/tmp/

pesto-run ${PESTO_APP} ${INPUT_FILE}

DATE=`date`
echo "# [${DATE}]"
