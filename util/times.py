import sys
import os

import h5py


def main():
    import argparse
    
    parser = argparse.ArgumentParser()

    parser.add_argument("fnames", 
                        nargs='+',
                        default=None,
                        help="Input files")

    args = parser.parse_args()

    for fname in args.fnames:
        fp = h5py.File(fname)
        print("t = %-20g   # %s" % (fp.attrs['t'], fname))


if __name__ == '__main__':
    main()
