// -*- C -*-
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif
  
#define _IDX(n_, i_, j_)  ((i_) * (n_) + (j_))

// We set the precision here
typedef DTYPE floatx;

__device__ __constant__ floatx e_mobility = 0.0372193;
__device__ __constant__ floatx e_charge = 1.6021766208e-19;
__device__ __constant__ floatx e_diffusion = 0.18;

__device__ __constant__ floatx ionization_alpha = 433200.0;
__device__ __constant__ floatx ionization_field = 2e7;

__device__ __constant__ floatx attachment_alpha = 2000.0;
__device__ __constant__ floatx attachment_field = 3e6;

const floatx EPSILON_0 = 8.854187817620389e-12;
const floatx MU_0 = 1.2566370614359173e-06;
const floatx CLIGHT = 2.99792e+08;

const floatx PI     = 3.141592653589793;
const floatx FOURPI = 12.566370614359172;

#define ONE_MU_0       (1 / MU_0)
#define ONE_EPSILON_0  (1 / EPSILON_0)

/* e * mu */
#define MINUS_INV_E          (-1 / e_charge)
#define E_ELECTRON_MOBILITY  (e_charge * e_mobility)

__device__ static floatx koren_limiter(floatx theta);
__device__ static floatx impact_nu(floatx eabs);
__device__ static floatx attachment_nu(floatx eabs);
__device__ static floatx threebody_nu(floatx eabs);
__device__ static floatx f3b(floatx x, floatx alpha, floatx beta);


/***********************************************************************
 *
 *  Handling Maxwell's equations
 *
 ***********************************************************************/


/* Updates the magnetic field.  Assumes that the ghost cells have been set 
   properly.

   Parameters:
   n, m, l      : Size of the physical domain and extra cells
   er, ez, hphi : the EM fields
   dt, dr, dz   : Timestep, mesh sizes
*/
  __global__ void update_h (int n, int m, int l,
			    const floatx * __restrict__ er,
			    const floatx * __restrict__ ez,
			    floatx * __restrict__ hphi,
			    floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i >=  n + 1 || j >= m + 1) return;
  
  int s = m + 2 * l + 1;

  int i1 = i + l;
  int j1 = j + l;

  hphi[_IDX(s, i1, j1)] +=
    ONE_MU_0 * ((dt / dr) * (ez[_IDX(s, i1, j1)] - ez[_IDX(s, i1 - 1, j1)]) -
		(dt / dz) * (er[_IDX(s, i1, j1)] - er[_IDX(s, i1, j1 - 1)]));
  
}



/* Updates the r-component of electric field and current.

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   er, hphi     : the EM fields (er and ez are overwritten)
   jr           : the electric currents (overwritten)
   ne           : the electron density
   dt, dr, dz   : Timestep, mesh sizes
*/
__global__ void update_er_jr (int n, int m, int l,
			      floatx * __restrict__ er,
			      const floatx * __restrict__ hphi,
			      floatx * __restrict__ jr,
			      const floatx * __restrict__ ne,
			      floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  
  if (i >= n + 1 || j >= m) return;

  int s = m + 2 * l + 1;

  /* Densities upwind, twice upwind and downwind. */
  floatx nup, nup2, ndown;
  floatx lhs, theta, sigma;
  
  int i1 = i + l;
  int j1 = j + l;

  // Get the diffusion current, which is independent of the field.
  floatx jdiff = (e_charge * e_diffusion *
		  (ne[_IDX(s, i1, j1)] - ne[_IDX(s, i1 - 1, j1)])
		  / dr);
  
  lhs = er[_IDX(s, i1, j1)] -
    dt * ONE_EPSILON_0 *
    ((1 / dz) * (hphi[_IDX(s, i1, j1 + 1)] - hphi[_IDX(s, i1, j1)]) +
     0.5 * jr[_IDX(s, i1, j1)] + 0.5 * jdiff);

  /* The sign of lhs is that of the current at the midpoint.  We use that to
     known the upwind direction at that time.
     Note tha j contains the electric current.  The electron velocity has the
     opposite sign (damn Franklin).
  */
  if (lhs > 0) {
    ndown = ne[_IDX(s, i1 - 1, j1)];
    nup   = ne[_IDX(s, i1,     j1)];
    nup2  = ne[_IDX(s, i1 + 1, j1)];
  } else {
    ndown = ne[_IDX(s, i1,     j1)];
    nup   = ne[_IDX(s, i1 - 1, j1)];
    nup2  = ne[_IDX(s, i1 - 2, j1)];
  }

  /* Here we are assuming a constant mobility but this can be generalized
     easily for a field-dependent mobility; we just have to inverse-interpolate.
     (see notes).
  */
  theta = (nup - nup2) / (ndown - nup);
  sigma = E_ELECTRON_MOBILITY * (nup + koren_limiter(theta) * (ndown - nup));

  er[_IDX(s, i1, j1)] = lhs / (1 + ONE_EPSILON_0 * sigma * dt / 2);
  jr[_IDX(s, i1, j1)] = er[_IDX(s, i1, j1)] * sigma + jdiff;

  /* Boundary conditions at the conducting surfaces. */
  if (j == 0) er[_IDX(s, i1, j1 - 1)] = -er[_IDX(s, i1, j1)];
  if (j == m - 1) er[_IDX(s, i1, j1 + 1)] = -er[_IDX(s, i1, j1)];
}

  
/* Updates the z-component of electric field and current.
   Note that this kernel and the previous one can be run
   concurrently.

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ez, hphi     : the EM fields (er and ez are overwritten)
   jr, jz       : the electric currents (overwritten)
   ne           : the electron density
   dt, dr, dz   : Timestep, mesh sizes
*/
__global__ void update_ez_jz (int n, int m, int l,
			      floatx * __restrict__ ez,
			      const floatx * __restrict__ hphi,
			      floatx * __restrict__ jz,
			      const floatx * __restrict__ ne,
			      floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i >= n || j >= m + 1) return;

  int s = m + 2 * l + 1;
  int i1 = i + l;
  int j1 = j + l;

  floatx nup, nup2, ndown;
  floatx lhs, theta, sigma;
  floatx r0, r1, rmid;
  
  r0    = i * dr;
  r1    = r0 + dr;
  rmid  = r0 + 0.5 * dr;
    
  // Get the diffusion current, which is independent of the field.
  floatx jdiff = (e_charge * e_diffusion *
		  (ne[_IDX(s, i1, j1)] - ne[_IDX(s, i1, j1 - 1)])
		  / dz);

  lhs = ez[_IDX(s, i1, j1)] +
    dt * ONE_EPSILON_0 *
    ((1.0 / dr / rmid) * (r1 * hphi[_IDX(s, i1 + 1, j1)] -
			  r0 * hphi[_IDX(s, i1,     j1)]) -
     0.5 * jz[_IDX(s, i1, j1)] + 0.5 * jdiff);

  if (lhs > 0.0) {
    ndown = ne[_IDX(s, i1, j1 - 1)];
    nup   = ne[_IDX(s, i1, j1    )];
    nup2  = ne[_IDX(s, i1, j1 + 1)];
  } else {
    ndown = ne[_IDX(s, i1, j1    )];
    nup   = ne[_IDX(s, i1, j1 - 1)];
    nup2  = ne[_IDX(s, i1, j1 - 2)];
  }

  theta = (nup - nup2) / (ndown - nup);
  sigma = E_ELECTRON_MOBILITY * (nup + koren_limiter(theta) * (ndown - nup));

  ez[_IDX(s, i1, j1)] = lhs / (1 + ONE_EPSILON_0 * sigma * dt / 2);
  jz[_IDX(s, i1, j1)] = ez[_IDX(s, i1, j1)] * sigma + jdiff;
  
  /* Set the boundary condition for the axis here.
     Performance-wise I am not sure if this is the most efficient way because
     we are leaving many threads inactive.  But on the other hand setting
     it here is so simple perhaps it compensates the overhead of launching
     a new context just to do this. */
  if (i == 0) ez[_IDX(s, i1 - 1, j1)] = ez[_IDX(s, i1, j1)];

}


__device__ static floatx koren_limiter(floatx theta)
{
  if      (theta >= 4)  return 1.0;
  else if (theta > 0.4) return 1.0 / 3.0 + theta / 6.0;
  else if (theta > 0.0) return theta;
  else                  return 0;
}


/* Update the absolute value of the electric field.  

   To implement photo-ionization we need |E| evaluated at the same times
   as n, which is midway between evaluations of E.  Therefore we track
   two arrays with |E| at the same times as E and at midpoints.

   This routine takes er and ez at time n and evaluates |E| at n (eabs)
   and updates |E| at n-1/2 (eabsmid).
   
   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   er, ez       : the electric field
   eabs         : Resulting |E| at n
   eabsmid      : Resulting |E| at n-1/2
   dr, dz       : mesh sizes
*/
__global__ void update_eabs (int n, int m, int l,
			     const floatx * __restrict__ er,
			     const floatx * __restrict__ ez,
			     floatx * __restrict__ eabs,
			     floatx * __restrict__ eabsmid)  
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i >= n || j >= m) return;
  
  int s = m + 2 * l + 1;
  int i1 = i + l;
  int j1 = j + l;

  floatx sumr = er[_IDX(s, i1, j1)] + er[_IDX(s, i1 + 1, j1    )];
  floatx sumz = ez[_IDX(s, i1, j1)] + ez[_IDX(s, i1,     j1 + 1)];
  floatx eabs_new = 0.5 * sqrt(sumr * sumr + sumz * sumz);
					     
  eabsmid[_IDX(s, i1, j1)] = 0.5 * (eabs[_IDX(s, i1, j1)] + eabs_new);
  eabs[_IDX(s, i1, j1)] = eabs_new;
}


/***********************************************************************
 *
 *  Handling the electron density
 *
 ***********************************************************************/

/* Update the electron density according to the transport and
   reactions equations.
   
   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   jr, jz       : the currents
   eabs         : the electric field
   n            : electron density
   dr, dz       : mesh sizes
*/
__global__ void update_n (int n, int m, int l,
			  const floatx * __restrict__ jr,
			  const floatx * __restrict__ jz,
			  const floatx * __restrict__ eabs,
			  floatx * __restrict__ ne,
			  floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i >= n || j >= m) return;
  
  int s = m + 2 * l + 1;
  int i1 = i + l;
  int j1 = j + l;

  floatx rc, rl, rr;
  rl = i * dr;
  rr = (i + 1) * dr;
  rc = 0.5 * (rl + rr);
  
  floatx eabs_ = eabs[_IDX(s, i1, j1)];
  
  floatx nu = impact_nu(eabs_) - attachment_nu(eabs_) - threebody_nu(eabs_);

  /* Note that j contains the electric current.  The particle current
     is -j / e. */
  floatx adv = (MINUS_INV_E *
		((dt / dr / rc) * (rl * jr[_IDX(s, i1,     j1)] -
				   rr * jr[_IDX(s, i1 + 1, j1)]) + 
		 (dt / dz) * (jz[_IDX(s, i1, j1)] -
			      jz[_IDX(s, i1, j1 + 1)])));
  // Crank-Nicolson
  ne[_IDX(s, i1, j1)] = ((adv +
			  (1.0 + 0.5 * nu * dt) * ne[_IDX(s, i1, j1)])
			 / (1.0 - 0.5 * nu * dt));
  
}

// Impact ionization rate (nu = kn)
__device__ static floatx
impact_nu(floatx eabs)
{
  return e_mobility * eabs * ionization_alpha * exp(-ionization_field / eabs);
}

// Dissociative attachment rate
__device__ static floatx
attachment_nu(floatx eabs)
{
  return e_mobility * eabs * attachment_alpha * exp(-attachment_field / eabs);
}

// 3-body attachment rate
__device__ static floatx
threebody_nu(floatx eabs)
{
  const floatx nair = 2.6867811e+25;

  const floatx alpha1 = 1.715;
  const floatx alpha2 = -0.905;

  const floatx beta1 = 0.328;
  const floatx beta2 = 1.295;

  const floatx eabs1 = 1.37563e5;
  const floatx eabs2 = 1.14161e6;
  const floatx eabs3 = 3.3746e3;

  const floatx a1 = 0.21 * 0.21 * nair * nair * 1.656e-42;
  const floatx a2 = 0.21 * 0.21 * nair * nair * 7.529e-43;

  return ((1 - exp(-pow(eabs / eabs3, 2)))
	  * (a1 * f3b(eabs / eabs1, alpha1, beta1) +
	     a2 * f3b(eabs / eabs2, alpha2, beta2)));
}


__device__ static floatx
f3b(floatx x, floatx alpha, floatx beta)
{
  return pow(x, -beta) / (1 + pow(x, alpha - beta));
}


/* Update the electron density according to the transport and
   reactions equations including photoionization.
   
   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ng           : number of groups
   jr, jz       : the currents
   eabs         : the electric field
   phi0         : The 0-order photon field.
   c            : the weight factors for each group.
   n            : electron density
   dr, dz       : mesh sizes
*/
__global__ void update_n_photo (int n, int m, int l, int ng,
				const floatx * __restrict__ jr,
				const floatx * __restrict__ jz,
				const floatx * __restrict__ eabs,
				const floatx * __restrict__ phi0,
				const floatx * __restrict__ c,
				floatx * __restrict__ ne,
				floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i >= n || j >= m) return;
  
  int s = m + 2 * l + 1;
  int i1 = i + l;
  int j1 = j + l;
  int si = n + 2 * l + 1;

  int shift = s * si;

  floatx rc, rl, rr;
  rl = i * dr;
  rr = (i + 1) * dr;
  rc = 0.5 * (rl + rr);
  
  floatx eabs_ = eabs[_IDX(s, i1, j1)];
  
  // This is only temporal.  Here we include only impact ionization.
  floatx nu = e_mobility * eabs_ *
    (ionization_alpha * exp(-ionization_field / eabs_) -
     attachment_alpha * exp(-attachment_field / eabs_));

  /* Note that j contains the electric current.  The particle current
     is -j / e. */
  floatx adv = (MINUS_INV_E *
		((dt / dr / rc) * (rl * jr[_IDX(s, i1,     j1)] -
				   rr * jr[_IDX(s, i1 + 1, j1)]) + 
		 (dt / dz) * (jz[_IDX(s, i1, j1)] -
			      jz[_IDX(s, i1, j1 + 1)])));

  floatx photo = 0;
  for (int g = 0; g < ng; g++, phi0 += shift) {
    photo += dt * c[g] * phi0[_IDX(s, i1, j1)];
  }
  
  // Crank-Nicolson
  ne[_IDX(s, i1, j1)] = ((adv + photo +
  			  (1.0 + 0.5 * nu * dt) * ne[_IDX(s, i1, j1)])
  			 / (1.0 - 0.5 * nu * dt));
  
}




/* Sets the upper electron density ghost cell values.

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ne           : the electron density
   a1, b1       : coefficients of the first ghost cell
   a2, b2       : coefficients of the second ghost cell
*/
  
__global__ void set_n_upper (int n, int m, int l,
			     floatx * __restrict__ ne,
			     floatx a1, floatx b1,
			     floatx a2, floatx b2)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  if (i >= n) return;

  int s = m + 2 * l + 1;
  int i1 = i + l;
  int j1 = m + l;

  ne[_IDX(s, i1, j1)] = (a1 * ne[_IDX(s, i1, j1 - 1)] +
			 b1 * ne[_IDX(s, i1, j1 - 2)]);

  ne[_IDX(s, i1, j1 + 1)] = (a2 * ne[_IDX(s, i1, j1 - 1)] +
			     b2 * ne[_IDX(s, i1, j1 - 2)]);
}


/* Sets the lower electron density ghost cell values.

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ne           : the electron density
   a1, b1       : coefficients of the first ghost cell
   a2, b2       : coefficients of the second ghost cell
*/
  
__global__ void set_n_lower (int n, int m, int l,
			     floatx * __restrict__ ne,
			     floatx a1, floatx b1,
			     floatx a2, floatx b2)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  if (i >= n) return;

  int s = m + 2 * l + 1;
  int i1 = i + l;
  int j1 = l - 1;

  ne[_IDX(s, i1, j1)] = (a1 * ne[_IDX(s, i1, j1 + 1)] +
			 b1 * ne[_IDX(s, i1, j1 + 2)]);

  ne[_IDX(s, i1, j1 - 1)] = (a2 * ne[_IDX(s, i1, j1 + 1)] +
			     b2 * ne[_IDX(s, i1, j1 + 2)]);
}


/* Sets the right electron density ghost cell values.

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ne           : the electron density
   a1, b1       : coefficients of the first ghost cell
   a2, b2       : coefficients of the second ghost cell
*/
  
__global__ void set_n_right (int n, int m, int l,
			     floatx * __restrict__ ne,
			     floatx a1, floatx b1,
			     floatx a2, floatx b2)
{
  int j = threadIdx.x + blockIdx.x * blockDim.x;
  if (j >= m) return;

  int s = m + 2 * l + 1;
  int i1 = n + l;
  int j1 = j + l;

  ne[_IDX(s, i1, j1)] = (a1 * ne[_IDX(s, i1 - 1, j1)] +
			 b1 * ne[_IDX(s, i1 - 2, j1)]);

  ne[_IDX(s, i1 + 1, j1)] = (a2 * ne[_IDX(s, i1 - 1, j1)] +
			     b2 * ne[_IDX(s, i1 - 2, j1)]);
}


/* Sets the right electron density ghost cell values.

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ne           : the electron density
   a1, b1       : coefficients of the first ghost cell
   a2, b2       : coefficients of the second ghost cell
*/
  
__global__ void set_n_left (int n, int m, int l,
			     floatx * __restrict__ ne,
			     floatx a1, floatx b1,
			     floatx a2, floatx b2)
{
  int j = threadIdx.x + blockIdx.x * blockDim.x;
  if (j >= m) return;

  int s = m + 2 * l + 1;
  int i1 = l - 1;
  int j1 = j + l;

  ne[_IDX(s, i1, j1)] = (a1 * ne[_IDX(s, i1 + 1, j1)] +
			 b1 * ne[_IDX(s, i1 + 2, j1)]);

  ne[_IDX(s, i1 - 1, j1)] = (a2 * ne[_IDX(s, i1 + 1, j1)] +
			     b2 * ne[_IDX(s, i1 + 2, j1)]);
}


  
/* Adds a gaussian source to ez.

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ez           : the EM field
   dr, dz       : Timestep, mesh sizes
   r0, z0       : Center of the gaussian
   a            : Amplitude of the gaussian
   width        : Width of the gaussian
*/
__global__ void gaussian_source (int n, int m, int l,
				 floatx * __restrict__ ez,
				 floatx dr, floatx dz,
				 floatx r0, floatx z0, floatx a, floatx width)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i >= n || j >= m + 1) return;
  
  int s = m + 2 * l + 1;
  int i1 = i + l;
  int j1 = j + l;

  floatx lr = (i + 0.5) * dr - r0;
  floatx lz = j * dz - z0;

  ez[_IDX(s, i1, j1)] += a * exp(-(lr * lr + lz * lz) / (width * width));
  
}


/***********************************************************************
 *
 *  CPML layer
 *
 ***********************************************************************/


/* Updates the magnetic field in the CPML layer.  

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   er, ez, hphi : the EM fields
   onemu        : 1 / mu
   dt, dr, dz   : Timestep, mesh sizes
*/
__global__ void cpml_update_h (int n, int m, int l,
			       const floatx * __restrict__ er,
			       const floatx * __restrict__ ez,
			       floatx * __restrict__ hphi,
			       floatx * __restrict__ psi_hphi_r,
			       floatx * __restrict__ af,
			       floatx * __restrict__ bf,
			       floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i >= l || j >= m + 1) return;
  
  // These are indices in the full arrays (er, ez, ...)
  int s = m + 2 * l + 1;
  int i1 = i + n + l + 1;
  int j1 = j + l;

  // And these are the indices in the psi array
  // Since this is a vertical array the LDA (Leading Dimension of the Array)
  // is also s.
  int s2 = s;
  int i2 = i;
  int j2 = j + l;

  floatx dez = (1 / dr) * (ez[_IDX(s, i1, j1)] - ez[_IDX(s, i1 - 1, j1)]);
  
  psi_hphi_r[_IDX(s2, i2, j2)] = (bf[i] * psi_hphi_r[_IDX(s2, i2, j2)] +
				  af[i] * ONE_MU_0 * dt * dez);

  hphi[_IDX(s, i1, j1)] +=
    ONE_MU_0 * (dt * dez -
		(dt / dz) * (er[_IDX(s, i1, j1)] - er[_IDX(s, i1, j1 - 1)]))
    + psi_hphi_r[_IDX(s2, i2, j2)];

}


/* Updates the r-component of the electric field inside the CPML. 

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   er, hphi     : the EM fields (er and ez are overwritten)
   dt, dr, dz   : Timestep, mesh sizes
*/
__global__ void cpml_update_er (int n, int m, int l,
				floatx * __restrict__ er,
				const floatx * __restrict__ hphi,
				floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;

  if (i >= l || j >= m) return;
  
  // These are indices in the full arrays (er, ez, ...)
  int s = m + 2 * l + 1;
  int i1 = i + n + l + 1;
  int j1 = j + l;

  er[_IDX(s, i1, j1)] -=  ONE_EPSILON_0 * (dt / dz) *
    (hphi[_IDX(s, i1, j1 + 1)] - hphi[_IDX(s, i1, j1)]);
}

/* Updates the r-component of the electric field inside the CPML. 

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   er, hphi     : the EM fields (er and ez are overwritten)
   dt, dr, dz   : Timestep, mesh sizes
*/
__global__ void cpml_update_ez (int n, int m, int l,
				floatx * __restrict__ ez,
				const floatx * __restrict__ hphi,
				floatx * __restrict__ psi_ez_r,
				floatx * __restrict__ ac,
				floatx * __restrict__ bc,
				floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;

  if (i >= l || j >= m + 1) return;
  
  // These are indices in the full arrays (er, ez, ...)
  int s = m + 2 * l + 1;
  int i1 = i + n + l;
  int j1 = j + l;
  
  int s2 = s;
  int i2 = i;
  int j2 = j + l;

  floatx r0, r1, rmid;
  r0    = (i + n + 0.5) * dr;
  r1    = r0 + dr;
  rmid  = r0 + 0.5 * dr;

  floatx dhphi = (1 / dr / rmid) *
    (r1 * hphi[_IDX(s, i1 + 1, j1)] - r0 * hphi[_IDX(s, i1, j1)]);

  psi_ez_r[_IDX(s2, i2, j2)] = (bc[i] * psi_ez_r[_IDX(s2, i2, j2)] +
				ac[i] * dt * ONE_EPSILON_0 * dhphi);
  
  ez[_IDX(s, i1, j1)] += dt * ONE_EPSILON_0 * dhphi + psi_ez_r[_IDX(s2, i2, j2)];

}


/***********************************************************************
 *
 *  Photo-ionization routines.
 *
 ***********************************************************************/

/* Updates the phi0 photon field
   
   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ng           : number of groups
   phi0         : The 0-order photon field.
   phi1r, phi1z : The 1-order photon field 
   eabs         : Electric field magnitude \
                                            > Neded for the photon source 
   ne           : Electron densitity       /
   alpha        : The absorption coefficients.
   dt, dr, dz   : timestep and mesh sizes
*/
__global__ void update_phi0 (int n, int m, int l, int ng,
			     floatx * __restrict__ phi0,
			     const floatx * __restrict__ phi1r,
			     const floatx * __restrict__ phi1z,
			     const floatx * __restrict__ ne,
			     const floatx * __restrict__ eabs,
			     const floatx * __restrict__ alpha,
			     floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  int g = threadIdx.z + blockIdx.z * blockDim.z;
  if (i >= n || j >= m || g >= ng) return;
  
  int s = m + 2 * l + 1;
  int si = n + 2 * l + 1;

  // We will operate only within a given group so we shift all our pointers
  // this length
  int shift = g * s * si;
  phi0 += shift;
  phi1r += shift;
  phi1z += shift;
  
  int i1 = i + l;
  int j1 = j + l;

  floatx rc, rl, rr;
  rl = i * dr;
  rr = (i + 1) * dr;
  rc = 0.5 * (rl + rr);

  
  /* First get the divergence of phi1 */
  floatx div_phi1 = ((1 / dr / rc) * (rr * phi1r[_IDX(s, i1 + 1, j1)] -
				      rl * phi1r[_IDX(s, i1, j1)]) +
		     (1 / dz) * (phi1z[_IDX(s, i1, j1 + 1)] -
				 phi1z[_IDX(s, i1, j1)]));

  floatx eabs_ = eabs[_IDX(s, i1, j1)];

  floatx iph = (ne[_IDX(s, i1, j1)] * e_mobility * eabs_
		* ionization_alpha * exp(-ionization_field / eabs_));
  
  phi0[_IDX(s, i1, j1)] =
    ((dt * (iph - CLIGHT * div_phi1 / 3)
      + (1 - alpha[g] * dt / 2) * phi0[_IDX(s, i1, j1)])
     / (1 + alpha[g] * dt / 2));

  if (i == 0) phi0[_IDX(s, i1 - 1, j1)] = phi0[_IDX(s, i1, j1)];
  // These are Marshak's b.c
  if (i == n - 1) {
    phi0[_IDX(s, i1 + 1, j1)] = phi0[_IDX(s, i1, j1)] *
      (1 - 3 * alpha[g] * dr / (4 * CLIGHT)) /
      (1 + 3 * alpha[g] * dr / (4 * CLIGHT));
  }
  if (j == 0) {
    phi0[_IDX(s, i1, j1 - 1)] = phi0[_IDX(s, i1, j1)] *
      (1 - 3 * alpha[g] * dz / (4 * CLIGHT)) /
      (1 + 3 * alpha[g] * dz / (4 * CLIGHT));
  }
  if (j == m - 1) {
    phi0[_IDX(s, i1, j1 + 1)] = phi0[_IDX(s, i1, j1)] *
      (1 - 3 * alpha[g] * dz / (4 * CLIGHT)) /
      (1 + 3 * alpha[g] * dz / (4 * CLIGHT));
  }
}


/* Updates the r-component of phi_1

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ng           : number of groups
   phi1r        : r-component of the first-degree photon field
   phi0         : 0-degree photon field.
   alpha        : Absorption
   dt, dr, dz   : Timestep, mesh sizes
*/
__global__ void update_phi1r(int n, int m, int l, int ng,
			     floatx * __restrict__ phi1r,
			     const floatx * __restrict__ phi0,
			     const floatx * __restrict__ alpha,
			     floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  int g = threadIdx.z + blockIdx.z * blockDim.z;
  if (i >= n + 1 || j >= m || g >= ng) return;

  int i1 = i + l;
  int j1 = j + l;

  int s = m + 2 * l + 1;
  int si = n + 2 * l + 1;

  int shift = g * s * si;
  phi0 += shift;
  phi1r += shift;

  floatx dphi0 = (phi0[_IDX(s, i1, j1)] - phi0[_IDX(s, i1 - 1, j1)]) / dr;
  phi1r[_IDX(s, i1, j1)] = ((- dt * CLIGHT * dphi0
  			      + (1 - alpha[g] * dt / 2) * phi1r[_IDX(s, i1, j1)])
  			     / (1 + alpha[g] * dt / 2));
}


/* Updates the z-component of phi_1

   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ng           : number of groups
   phi1z        : z-component of the first-degree photon field
   phi0         : 0-degree photon field.
   alpha        : Absorption
   dt, dr, dz   : Timestep, mesh sizes
*/
__global__ void update_phi1z(int n, int m, int l, int ng,
			     floatx * __restrict__ phi1z,
			     const floatx * __restrict__ phi0,
			     const floatx * __restrict__ alpha,
			     floatx dt, floatx dr, floatx dz)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  int g = threadIdx.z + blockIdx.z * blockDim.z;
  if (i >= n || j >= m + 1 || g >= ng) return;

  int i1 = i + l;
  int j1 = j + l;

  int s = m + 2 * l + 1;
  int si = n + 2 * l + 1;

  int shift = g * s * si;
  phi0 += shift;
  phi1z += shift;

  floatx dphi0 = (phi0[_IDX(s, i1, j1)] - phi0[_IDX(s, i1, j1 - 1)]) / dz;
  phi1z[_IDX(s, i1, j1)] = ((- dt * CLIGHT * dphi0
			     + (1 - alpha[g] * dt / 2) * phi1z[_IDX(s, i1, j1)])
			    / (1 + alpha[g] * dt / 2));
}


/* Sums the phi0 photon field (debugging only)
   
   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   ng           : number of groups
   phi0         : The 0-order photon field.
   c            : the weight factors for each group.
   sphoto       : the source of electrons due to photo-ionization.

*/
__global__ void sum_phi0 (int n, int m, int l, int ng,
			  const floatx * __restrict__ phi0,
			  const floatx * __restrict__ c,
			  floatx * __restrict__ sphoto)
{
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  int j = threadIdx.y + blockIdx.y * blockDim.y;
  if (i >= n || j >= m) return;
  
  int i1 = i + l;
  int j1 = j + l;

  int s = m + 2 * l + 1;
  int si = n + 2 * l + 1;

  int shift = s * si;
  
  sphoto[_IDX(s, i1, j1)] = 0;
  
  for (int g = 0; g < ng; g++, phi0 += shift) {
    sphoto[_IDX(s, i1, j1)] += c[g] * phi0[_IDX(s, i1, j1)];
  }
}



/***********************************************************************
 *
 *  Near-to-far-field (NTFF) transform
 *
 ***********************************************************************/



/* Calculate the matrix for the NTTF transform.
   
   Parameters:

   n, m, l      : Size of the physical domain and extra cells
   tlen         : Number of possible time instants affected by a near-field
   tobsmin      : The earliest possibly affected time.
   ntheta       : Number of points in the axial direction.
   azimuthal    : true if we are integrating an azimuthal field (hphi)
   auw          : The matrix that we are constructing
   dt, dr, dz   : timestep and mesh sizes
*/
__global__ void calc_ntff (int n, int m, int l,
			   int tlen, floatx tobsmin,
			   int ntheta,
			   int azimuthal,
			   floatx * __restrict__ auw,
			   floatx robs, floatx zobs,
			   floatx dt, floatx dr, floatx dz)
{
  // i is here the index into the vertical dimension of the physical domain.
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  if (i >= m + 1) return;

  // With w we take into account that the first and last points are weighted
  // as 1/2 in the trapezoid method.
  floatx w = (i == 0 || i == m) ? 0.5: 1.0;

  floatx r = sqrt(zobs * zobs + robs * robs);
  floatx z1 = i * dz;
  floatx r1 = n * dr;
  floatx dtheta = 2 * PI / ntheta;
    
  for (int j = 0; j < ntheta; j++) {
    // TODO: make use of the symmetry theta ~ 2 pi - theta.
    floatx theta = 2 * PI * j / ntheta;
    floatx x1 = r1 * cos(theta);
    // This is not really needed anywhere:
    // floatx y1 = r1 * sin(theta);
    
    // Following Taflove, this is the first-order approximation to the
    // distance between the source and the observation points.
    floatx f = (r - (x1 * robs + z1 * zobs) / r) / CLIGHT / dt;
    int p = (int) floor(f - tobsmin / dt);

    if (p < 0 || p >= tlen) {
      // If the arrays are correctly allocated this should never happen
      // but we just silently ignore values outside the allocated array.
      continue;
    }
    
    floatx a = f - floor(f);
    floatx v = w * r1 * dz * dtheta / (FOURPI * CLIGHT * r * dt);
    if (azimuthal) v *= cos(theta);
		     
    auw[_IDX(m + 1, p    , i)] += (1 - a) * v;
    auw[_IDX(m + 1, p + 1, i)] += (2 * a - 1) * v;
    auw[_IDX(m + 1, p + 2, i)] += -a * v;
  }
    
}


#ifdef __cplusplus
}
#endif
