""" Interface with cuda.  

Here we import all required functions from the compiled .cubin module and
the pycuda interface.
"""
import os
import sys

import numpy as np

from pycuda.autoinit import device, context
import pycuda.driver as drv
import pycuda.gpuarray as gpu
import pycuda.curandom as curandom


class Module():
    def __init__(self, dtype):
        self.cuda_mod = drv.module_from_file(
            os.path.join(os.path.abspath(os.path.dirname(__file__)),
                         "fdtd-%s.cubin" % np.dtype(dtype).name))
        self.dtype = dtype
        
        for func in ["update_h",
                     "update_er_jr",
                     "update_ez_jz",
                     "update_eabs",
                     "cpml_update_h",
                     "cpml_update_er",
                     "cpml_update_ez",
                     "update_n",
                     "update_n_photo",
                     "set_n_upper",
                     "set_n_lower",
                     "set_n_left",
                     "set_n_right",
                     "gaussian_source",
                     "update_phi0",
                     "update_phi1r",
                     "update_phi1z",
                     "sum_phi0",
                     "calc_ntff"]:
            setattr(self, func, self.cuda_mod.get_function(func))

    
    def set_parameter(self, name, value, dtype=None):
        """ Sets a module parameter defined in constant memory. """
        if dtype is None:
            dtype = self.dtype

        ptr  = self.cuda_mod.get_global(name)[0]
        print("[MOD] %s = %s (%s)" % (name, dtype(value), np.dtype(dtype).name))
        
        drv.memcpy_htod(ptr, dtype(value))


modules = {dtype: Module(dtype) for dtype in (np.float32, np.float64)}

def init_parameters(params):
    """ Inits all __constant__ parameters in the module. """
    param_names = ["e_mobility",
                   "e_charge",
                   "e_diffusion",
                   "ionization_alpha",
                   "ionization_field",
                   "attachment_alpha",
                   "attachment_field"]
    
    print(params.asdict())
    for name in param_names:
        val = params.asdict().get(name, None)
        print(name, val)
        if val is None:
            continue
        
        for dtype, mod in modules.items():
            type_conversion = {
                float: dtype,
                int: np.int32}
            
            param_type = type_conversion.get(type(val), type(val))
            mod.set_parameter(name, val, param_type)

    
