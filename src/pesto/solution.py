from collections import namedtuple

import numpy as np

GPUVars = namedtuple("GPUVars",
                     ["ne", "er", "jr", "ez", "jz", "hphi",
                      "eabs", "eabsmid",
                      "psi_hphi_r", "psi_ez_r"])

class Solution():
    def __init__(self, dom, dtype=np.float64):
        self.dom = dom
        self.dtype = dtype
        self.gpuvars = None

        self.data_reduction = 1
        
    def to_gpu(self):
        """
        Moves all data to the GPU.
        """
        from .cuda import gpu

        if self.gpuvars is None:
            fields = []
            for v in GPUVars._fields:
                fields.append(gpu.to_gpu(getattr(self, v)))

            self.gpuvars = GPUVars(*fields)

        else:
            for v in GPUVars._fields:
                getattr(self.gpuvars, v)[:, :] = getattr(self, v)


    def from_gpu(self):
        """
        Retrieves all data from the GPU.
        """
        for v in GPUVars._fields:
            getattr(self.gpuvars, v).get_async(ary=getattr(self, v))
            
        
    def setall(self, **kwargs):
        """ 
        Inits all arrays on the GPU.  Those that are not given in **kwargs
        are initialized as zero. 
        """
        dom = self.dom
        nx, mx = dom.nx, dom.mx
        
        # Variables defined in the full domain.  All of them have the same size
        # even if the physical arrays are smaller; we leave some points
        # unused but thus we improve memory coalescence and simplicity.
        for var in ["ne", "er", "jr", "ez", "jz", "hphi", "eabs", "eabsmid"]:
            try:
                value = kwargs[var]
                if np.isscalar(value):
                    v = dom.zeros_host(self.dtype)
                    v[:, :] = value
                    setattr(self, var, v)
                    
                else:
                    if value.shape != (nx, mx):
                        raise ValueError("%s must have shape (%d, %d)"
                                         % (var, nx, mx))                    
                    setattr(self, var, value.astype(self.dtype).copy())

            except KeyError:
                setattr(self, var, dom.zeros_host(self.dtype))


        # These are defined only on the CPML
        for var in ["psi_ez_r", "psi_hphi_r"]:
            try:
                value = kwargs[var]
                if value.shape != (l, mx):
                    raise ValueError("%s must have shape (%d, %d)"
                                     % (var, l, mx))
                setattr(self, var, value.astype(self.dtype).copy())

            except KeyError:
                setattr(self, var, dom.zeros_host_l(self.dtype))
            
        
    def save(self, group):
        self.from_gpu()
        skip = self.data_reduction
        
        for var in GPUVars._fields:
            group.create_dataset(var,
                                 data=getattr(self, var)[::skip, ::skip],
                                 compression='gzip')

        group.attrs['data_reduction'] = self.data_reduction

        
    def load(self, group, togpu=False):
        for var in GPUVars._fields:
            try:
                getattr(self, var)[:, :] = np.array(group[var])
            except AttributeError:
                setattr(self, var, np.array(group[var]).copy())
            except KeyError:
                # Do nothing.  Let's hope that we won't need this variable later
                pass
