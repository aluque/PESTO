import os
import time

import numpy as np
import h5py

class Controller():
    BC = {
        'extrapol0': [1,  0, 1,  0],
        'extrapol1': [2, -1, 3, -2],
        'symmetric': [1,  0, 0,  1],
        'dirichlet': [-1, 0, 0, -1],
        'zero':      [0,  0, 0,  0]}

    
    def __init__(self, dom, sol, photo=None, ntff=[]):
        self.dom = dom
        self.sol = sol
        
        self.bc_top    = [sol.dtype(x) for x in self.BC['extrapol0']]
        self.bc_bottom = [sol.dtype(x) for x in self.BC['extrapol0']]
        self.bc_left   = [sol.dtype(x) for x in self.BC['extrapol0']]
        self.bc_right  = [sol.dtype(x) for x in self.BC['extrapol0']]

        self.photo = photo
        self.ntff = ntff.copy()

        
    def init_output(self, outdir, params={}):
        try:
            os.mkdir(outdir)
        except OSError:
            pass        

        self.outdir = outdir

        
    def run(self, out_times, start=0, **kwargs):
        self.dom.init_cpml(self.sol.dtype)
        if self.ntff:
            ofile = h5py.File(os.path.join(self.outdir, "ntff_mat.h5"), "w")
            for i, ntff in enumerate(self.ntff):
                ntff.init()
                group = ofile.create_group('%.4d' % i)
                ntff.dump(group)

            ofile.close()
            
                
        dt = self.dom.dt
        self.t = float(start)
        tinit = time.time()
        
        for step, tnext in enumerate(out_times):
            n = int(np.ceil((tnext - self.t) / dt))
            t0 = time.time()
            
            self.step(n, **kwargs)
            self.save(step, self.t)

            t1 = time.time()
            etime = t1 - t0
            if self.t > start:
                eta = t1 + ((out_times[-1] - self.t) * (t1 - tinit)
                            / (self.t - start))
            else:
                eta = -1
                
            speed = (self.t - start) / (t1 - tinit)

            
            print("%s:" % time.ctime())
            print("               t       = %-28g   # %s"
                  % (self.t, 'Simulation time'))
            print("               etime   = %-28f   # %s"
                  % (etime, 'Time elapsed since last output (s)'))
            print("               ttime   = %-28d   # %s"
                  % ((t1 - tinit), 'Wall time since start (s)'))
            print("               speed   = %-28g   # %s"
                  % (speed, 'Simulation time / wall time'))
            print("               eta     = %-28d   # %s"
                  % ((eta - t1), 'Estimated time to completion (s)'))
            print("               ftime   = %-28s   # %s"
                  % (repr(time.ctime(eta)), 'Estimated moment of completion'))
            print()

            
    def step(self, nsteps, blk=(1, 256, 1)):
        """ 
        Performs n FDTD steps.
        """
        from . import cuda

        n, m, l = self.dom.n, self.dom.m, self.dom.l
        dt, dr, dz = self.dom.dt, self.dom.dr, self.dom.dz
        ne = self.sol.gpuvars.ne
        ez = self.sol.gpuvars.ez
        er = self.sol.gpuvars.er
        eabs = self.sol.gpuvars.eabs
        eabsmid = self.sol.gpuvars.eabsmid
        jz = self.sol.gpuvars.jz
        jr = self.sol.gpuvars.jr
        hphi = self.sol.gpuvars.hphi
        psi_hphi_r = self.sol.gpuvars.psi_hphi_r
        psi_ez_r = self.sol.gpuvars.psi_ez_r

        # Photoionization vars
        if self.photo is not None:
            alpha = self.photo.gpuvars.alpha
            C = self.photo.gpuvars.C
            phi0 = self.photo.gpuvars.phi0
            phi1r = self.photo.gpuvars.phi1r
            phi1z = self.photo.gpuvars.phi1z
            ng = self.photo.ng
            blk_photo = (blk[0], blk[1], ng)

        
        ac, bc = self.dom.ac, self.dom.bc
        af, bf = self.dom.af, self.dom.bf
        
        floatx = self.sol.dtype
        mod = cuda.modules[self.sol.dtype]
        
        for i in range(nsteps):
            ###
            ### i -> i + 1 STEP
            ###

            # self.t is the center time of the differences
            self.t += dt / 2

            mod.update_h(np.int32(n), np.int32(m), np.int32(l),
                         er, ez, hphi,
                         floatx(dt), floatx(dr), floatx(dz),
                         block=blk,
                         grid=(int(np.ceil((n + 1) / blk[0])),
                               int(np.ceil((m + 1) / blk[1])), 1))

            mod.cpml_update_h(np.int32(n), np.int32(m), np.int32(l),
                              er, ez, hphi, psi_hphi_r,  af, bf, 
                              floatx(dt), floatx(dr), floatx(dz),
                              block=blk,
                              grid=(int(np.ceil(l / blk[0])),
                                    int(np.ceil((m + 1) / blk[1])), 1))

            if self.photo is not None:
                mod.update_n_photo(np.int32(n), np.int32(m), np.int32(l),
                                   np.int32(ng),
                                   jr, jz, eabs, phi0, C, ne,
                                   floatx(dt), floatx(dr), floatx(dz),
                                   block=blk, 
                                   grid=(int(np.ceil(n / blk[0])),
                                         int(np.ceil(m / blk[1])), 1))
            else:
                mod.update_n(np.int32(n), np.int32(m), np.int32(l),
                             jr, jz, eabs, ne,
                             floatx(dt), floatx(dr), floatx(dz),
                             block=blk, 
                             grid=(int(np.ceil(n / blk[0])),
                                   int(np.ceil(m / blk[1])), 1))
                

            mod.set_n_upper(np.int32(n), np.int32(m), np.int32(l),
                             ne,
                             *self.bc_top,
                             block=(256, 1, 1),
                             grid=(int(np.ceil(n / 256)), 1, 1))

            mod.set_n_lower(np.int32(n), np.int32(m), np.int32(l),
                            ne,
                            *self.bc_bottom,
                            block=(256, 1, 1),
                            grid=(int(np.ceil(n / 256)), 1, 1))
            
            mod.set_n_right(np.int32(n), np.int32(m), np.int32(l),
                            ne,
                            *self.bc_right,
                            block=(256, 1, 1),
                            grid=(int(np.ceil(m / 256)), 1, 1))

            mod.set_n_left(np.int32(n), np.int32(m), np.int32(l),
                            ne,
                            *self.bc_left,
                            block=(256, 1, 1),
                            grid=(int(np.ceil(m / 256)), 1, 1))
            
            if self.photo is not None:
                mod.update_phi1r(np.int32(n), np.int32(m), np.int32(l),
                                 np.int32(ng),
                                 phi1r, phi0, alpha,
                                 floatx(dt), floatx(dr), floatx(dz),
                                 block=blk_photo,
                                 grid=(int(np.ceil((n + 1) / blk[0])),
                                       int(np.ceil(m  / blk[1])), 1))
                              
                mod.update_phi1z(np.int32(n), np.int32(m), np.int32(l),
                                 np.int32(ng),
                                 phi1z, phi0, alpha,
                                 floatx(dt), floatx(dr), floatx(dz),
                                 block=blk_photo,
                                 grid=(int(np.ceil(n / blk[0])),
                                       int(np.ceil((m + 1) / blk[1])), 1))
            
            ###
            ### i + 1/2 -> i + 3/2 STEP
            ###
            self.t += dt / 2

            mod.update_er_jr(np.int32(n), np.int32(m), np.int32(l),
                             er, hphi, jr, ne,
                             floatx(dt), floatx(dr), floatx(dz),
                             block=blk, 
                             grid=(int(np.ceil((n + 1) / blk[0])),
                                   int(np.ceil(m / blk[1])), 1))

            mod.cpml_update_er(np.int32(n), np.int32(m), np.int32(l),
                               er, hphi, 
                               floatx(dt), floatx(dr), floatx(dz),
                               block=blk, 
                               grid=(int(np.ceil(l / blk[0])),
                                     int(np.ceil(m / blk[1])), 1))

            mod.update_ez_jz(np.int32(n), np.int32(m), np.int32(l),
                             ez, hphi, jz, ne,
                             floatx(dt), floatx(dr), floatx(dz),
                             block=blk, 
                             grid=(int(np.ceil(n / blk[0])),
                                   int(np.ceil((m + 1) / blk[1])), 1))

            mod.cpml_update_ez(np.int32(n), np.int32(m), np.int32(l),
                               ez, hphi, psi_ez_r, ac, bc,
                               floatx(dt), floatx(dr), floatx(dz),
                               block=blk, 
                               grid=(int(np.ceil(l / blk[0])),
                                     int(np.ceil((m + 1) / blk[1])), 1))

            mod.update_eabs(np.int32(n), np.int32(m), np.int32(l),
                            er, ez, eabs, eabsmid,
                            block=blk, 
                            grid=(int(np.ceil(n / blk[0])),
                                  int(np.ceil(m / blk[1])), 1))
            
            if self.photo is not None:
                mod.update_phi0(np.int32(n), np.int32(m), np.int32(l),
                                np.int32(ng),
                                phi0, phi1r, phi1z, ne, eabsmid, alpha,
                                floatx(dt), floatx(dr), floatx(dz),
                                block=blk_photo,
                                grid=(int(np.ceil(n / blk[0])),
                                      int(np.ceil(m / blk[1])), 1))
            
            for ntff in self.ntff:
                ntff.apply(self.t - dt / 2, self.t, ez, hphi)
                
                
    def save(self, step, t):
        ofile = h5py.File(os.path.join(self.outdir, "%.5d.h5" % step), "w")

        domain_group = ofile.create_group('domain')
        steps_group  = ofile.create_group('steps')
        ntff_group   = ofile.create_group('ntff')
        if self.ntff:
            ez = np.stack([i.ezobs.get_async() for i in self.ntff])
            
            ntff_group.create_dataset('t', data=self.ntff[0].tobs,
                                      compression='gzip')

            ntff_group.create_dataset('ez', data=ez,
                                      compression='gzip')
            
        self.dom.save(domain_group)

        ofile.attrs['_timestamp_'] = time.time()
        ofile.attrs['t'] = t
        
        self.sol.save(ofile)

        
