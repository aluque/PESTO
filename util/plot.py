#!/usr/bin/env python3

import sys
import os
import logging


# SET UP LOGGING

logger = logging.getLogger('em')
logger.setLevel(logging.DEBUG)

fh = logging.FileHandler('plot.log')
fh.setLevel(logging.INFO)
formatter = logging.Formatter("[%(asctime)s] %(message)s",
                              "%a, %d %b %Y %H:%M:%S")
fh.setFormatter(formatter)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)

logger.addHandler(fh)
logger.addHandler(ch)
logging.captureWarnings(True)


def get_parser():
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="HDF5 input file")

    parser.add_argument("var", 
                        nargs='?',
                        default=None,
                        help="Variable to plot")

    parser.add_argument("-o", "--output",
                        help="Output file (may contain {rid} {step} and {var})", 
                        action='store', default='{var}_{step}.png')

    parser.add_argument("-d", "--outdir",
                        help="Output directory (may contain {rid} {step} and {var})", 
                        action='store', default='plots')

    parser.add_argument("--clim", "-c",
                        help="Limits of the color scale (c0:c1)", 
                        action='store', default=None)

    parser.add_argument("--rlim", "-r",
                        help="Limits of the r-axis (r0:r1)", 
                        action='store', default=None)

    parser.add_argument("--zlim", "-z",
                        help="Limits of the z-axis (z0:z1)", 
                        action='store', default=None)

    parser.add_argument("--cmap",
                        help="Colormap to use", 
                        action='store', default="inferno")
    
    parser.add_argument("--diff",
                        help="Plot difference with this step", 
                        action='store', default=None)

    parser.add_argument("--figsize",
                        help="Size of the figure as width,height", 
                        action='store', default=None)

    parser.add_argument("--portrait",
                        help="Use portrait mode", 
                        action='store_true')
    
    parser.add_argument("--contour", action='store_true',
                        help="Plot contour lines")

    parser.add_argument("--log", action='store_true',
                        help="Logarithmic scale?")

    parser.add_argument("--abs", action='store_true',
                        help="Plot absolute value")

    parser.add_argument("--mirror", action='store_true',
                        help="Add a mirror image.")

    parser.add_argument("--update", "-u", action='store_true',
                        help="Do something only if the output file does not exist.")
    
    parser.add_argument("--offset", action='store', type=float,
                        help="Substract this offset from the data")

    parser.add_argument("--negative", "-n", action='store_true', default=False,
                        help="Plot minus the data")

    parser.add_argument("--skip", action='store', type=int, default=1,
                        help="Reduce the data length with steps of this size")

    parser.add_argument("--show", help="Interactively show the figure", 
                        action='store_true', default=False)

    return parser


def main():
    parser = get_parser()
                        
    args = parser.parse_args()

    inpath = os.path.split(os.path.abspath(args.input))[0]
    rid  = os.path.split(inpath)[1]
    step = os.path.splitext(os.path.split(args.input)[1])[0]
    outdir = os.path.join(inpath, args.outdir.format(rid=rid, var=args.var))

    ofile = args.output.format(step=step, var=args.var, rid=rid)
    ofile = os.path.join(outdir, ofile)

    if not args.show and args.update and os.path.exists(ofile):
        logger.info("Skipping %s" % ofile)
        sys.exit(0)

    import numpy as np
    import h5py
    from matplotlib import pyplot as plt
    from matplotlib.colors import LogNorm
    from matplotlib import cm
    
    import scipy.constants as co

    from pesto import Domain, Solution, Variable

    fp = h5py.File(args.input, "r")

    dom = Domain.load(fp['domain'])
    sol = Solution(dom)
    sol.load(fp)
    time = fp.attrs['t']
    fp.close()
    
    
    logger.info("Step %s loaded (t = %g)" % (step, time))

    variable = Variable.INDEX[args.var]
    r, z, ary = variable(sol)

    if args.diff is not None:
        if args.diff[0] == '+' or args.diff[0] == '-':
            fname = "%.5d.h5" % (int(step) + int(args.diff))
        else:
            fname = args.diff
            
        fp = h5py.File(fname, "r")
        sol2 = Solution(dom)
        sol2.load(fp)
        fp.close()
        _, _, ary2 = variable(sol2)
        ary -= ary2
    
        
    try:
        os.mkdir(outdir)
    except OSError:
        pass

    kwargs = {}
    if args.clim is not None:
        clim = [float(x) for x in args.clim.split(':')]
        kwargs['vmin'] = clim[0]
        kwargs['vmax'] = clim[1]

    if args.log:
        kwargs['norm'] = LogNorm()
        ary += 1e-8
    
    kwargs['cmap'] = cm.get_cmap(args.cmap)

    if args.offset:
        ary -= args.offset

    if args.abs:
        ary = np.abs(ary)
        
    if args.negative:
        ary = -ary
    
    if args.rlim is not None:
        rlim = [float(x) for x in args.rlim.split(':')]
        rfilter = np.logical_and(rlim[0] < r, r < rlim[1])
        ary = ary[rfilter, :]
        r = r[rfilter]
        
    if args.zlim is not None:
        zlim = [float(x) for x in args.zlim.split(':')]
        zfilter = np.logical_and(zlim[0] < z, z < zlim[1])
        ary = ary[:, zfilter]
        z = z[zfilter]
        

    if args.mirror:
        ary = np.concatenate((ary[::-1, :], ary), axis=0)
        r = np.concatenate((-r[::-1], r), axis=0)

    if args.portrait:
        ary = ary.T
        z, r = r, z

    if args.figsize:
        width, height = [float(v) for v in args.figsize.split(',')]
        plt.figure(figsize=(width, height))

    # width  = 8
    # height = 9 * ary.shape[0] / ary.shape[1]
        
    plt.pcolor(z[::args.skip], r[::args.skip],
               ary[::args.skip, ::args.skip], **kwargs)
    cbar = plt.colorbar()
    cbar.set_label("%s ($\\mathdefault{%s}$)"
                   % (variable.name, variable.units))
    
    if args.contour:
        kwargs['cmap'] = None
        plt.contour(z[::args.skip], r[::args.skip],
                    ary[::args.skip, ::args.skip], colors='w', alpha=0.5,
                    **kwargs)
        
    plt.axis('scaled')

    if not args.portrait:
        plt.xlabel("$z$ (m)")
        plt.ylabel("$r$ (m)")
    else:
        plt.ylabel("$z$ (m)")
        plt.xlabel("$r$ (m)")
        
        
    plt.title("t = %.4f ns" % (time / co.nano))
    plt.gcf().tight_layout()
    
    if not args.show:
        plt.savefig(ofile,dpi=500)
        plt.close()
        logger.info("File '%s' saved" % ofile)
    else:
        plt.show()

    
if __name__ == '__main__':
    main()

